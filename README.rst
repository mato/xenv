xenv: eXtended ENV
==================

xenv consists in classes and functions to manipulate the environment in a
platform independent way.

In particular it allows to describe the changest to the environment in an XML
file that can be used by the script ``xenv`` (similar to the Unix ``env``
command) to run a command in the modified environment.
